import React from 'react'
import MessageList from './MessageList'
import SendMessageForm from './SendMessageForm'

class ContactDetail extends React.Component {

    constructor(props) {
        super(props);
   
        };

    render() {
        return (
          <div className="chat">        
            <MessageList contactId={this.props.location.state.contactId} />
            <SendMessageForm />
         </div>
        )
      }
 
}

export default ContactDetail