import React from 'react'

class ContactList extends React.Component {

    
    constructor(props) {
        super(props);
        this.state = {
            contacts: [],
            contactId: ''
        };

        this.getDetail = this.getDetail.bind(this);

        console.log(this.props);

        console.log(this.props.history)
    }
    



    componentDidMount() {

        fetch('http://chat-server-challange.herokuapp.com/contacts', {
            credentials: 'include',
        })
            .then(response => response.json())
            .then(items => {

                this.setState({
                    contacts: items
                });

            })
            .catch(error => console.error(error))

    }

/*
 getDetail() {

    //this.setState({"contactId": val.id});
    this.props.history.push('/detail');
 }
 */


getDetail(ev) {

    let id = ev.currentTarget.dataset.contact_id;

    this.props.history.push({
        pathname: '/detail',
        state: { contactId: id }
      })

  }
 

    render() {

        let items = this.state.contacts;


        return (
    
            <div className="outer-container">
                <div className="inner-container">
                    <div className="centered-content_img">
                        <ul>
                            {this.state && this.state.contacts &&
                                items.map((val, index) => {
                               
                                    return (
                                        <li key={index} className="no-dot">
                                           
                                            <img src={val.avatar} data-contact_id={val.id} onClick={this.getDetail}  alt={val.username} className="img_width"></img>
                                            <br />
                                            {val.name}
                                            <br />
                                            {val.unreadCount}
                                        </li>
                                    );
                                })
                            }
                        </ul>

                    </div>
                </div>
            </div>

        )

    }



}

export default ContactList