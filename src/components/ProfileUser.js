import React from 'react'


class ProfileUser extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            profile: {}
        };
    }


    componentDidMount() {

        fetch('http://chat-server-challange.herokuapp.com/profile', {
            credentials: 'include',
        })
            .then(response => response.json())
            .then(item => {

                this.setState({
                    profile: item
                });

            })
            .catch(error => console.error(error))

    }

    render() {

        let profile = this.state.profile;

        return (
            <div className="outer-container">
                <div className="inner-container">
                    <div className="centered-content-img">

                        {this.state && this.state.profile &&

                            <div>
                                <div><img src={profile.avatar} className="img_width"></img></div>
                                <div>{profile.username}</div>
                                <div>{profile.name}  {profile.name}</div>
                                <div>{profile.email}</div>
                            </div>
                        }
                    </div>
                </div>
            </div>

        )

    }
}

export default ProfileUser