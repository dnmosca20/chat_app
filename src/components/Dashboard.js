import React from 'react'
import './Dashboard.css';
import ContactList from "./ContactList.js";
import ProfileUser from "./ProfileUser.js";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import "react-tabs/style/react-tabs.css";


class Dashboard extends React.Component {

    constructor(props) {
        super(props);

        console.log(this.props);
    }

    render() {

        return (

            <Tabs defaultIndex={0}>
                <TabList>
                    <Tab >Contacts</Tab>
                    <Tab>Profile</Tab>
                </TabList>

                <TabPanel>
                    <ContactList history= {this.props.history} />
                </TabPanel>
                <TabPanel>
                    <ProfileUser />
                </TabPanel>
            </Tabs>


        )




    }



}

export default Dashboard