import React from 'react'

const DUMMY_DATA = [
    {
        senderId: 'Tizio',
        text: 'Ciao, come va?'
    },
    {
        senderId: 'Caio',
        text: 'Bene, tu?'
    },
    {
        senderId: 'Tizio',
        text: 'Sono contento !'
    }
]

class MessageList extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            messages: [],
            contactId: this.props.contactId
        };


        //console.log(props.location.state.contactId)
    }


    componentDidMount() {


        let contactId = this.props.contactId;

        fetch('http://chat-server-challange.herokuapp.com/contacts/' + this.state.contactId + '/history', {
            credentials: 'include',
        })
            .then(response => response.json())
            .then(js => {

                this.setState({
                    messages: js.messages
                });

            })
            .catch(error => console.error(error))

    }



    render() {

        let items = this.state.messages;

        return (

            /*
              <div className="message-list">           
                  
                     { DUMMY_DATA.map((message, index) => {
                      return (
                          
                          <div key={index} className="message">
                              <div className="message-username">{message.senderId}</div>
                              <div className="message-text">{message.text}</div>
                          </div>
                      )
                  })}
                  
              </div>
              */

              
             <div className="message-list">      
                {
                    this.state && this.state.messages &&
                    
                    items.map((msg, index) => {

                        return (
                            <div key={index} className="message">
                                <div className="message-username">{msg.contactId}</div>
                                <div className="message-text">{msg.message}</div>
                            </div>
                        );
                    })
                    
                }

            </div>
        )
    }
}

export default MessageList