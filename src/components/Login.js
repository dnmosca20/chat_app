import React from 'react'
import './Login.css';

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      error: '',
    };

    this.handlePassChange = this.handlePassChange.bind(this);
    this.handleUserChange = this.handleUserChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.dismissError = this.dismissError.bind(this);

  }

  dismissError() {
    this.setState({ error: '' });
  }

  handleSubmit(evt) {
    evt.preventDefault();

    if (!this.state.username) {
      return this.setState({ error: 'Username is required' });
    }

    if (!this.state.password) {
      return this.setState({ error: 'Password is required' });
    }


    this.login();


  }

  login() {

    let obj = {};

    obj.username = this.state.username;
    obj.password = this.state.password;

    let data = JSON.stringify(obj);

    fetch("http://chat-server-challange.herokuapp.com/login", {
      credentials: 'include',
      //credentials: 'same-origin',
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
      , body: data
    })
      .then(res => {
        if (res.ok) {

          console.log(res.headers.get('set-cookie'));
          console.log(document.cookie);
          return res.json();

        } else {
          throw Error(res.statusText);
        }
      })
      .then(json => {

        console.log(json);

        this.props.history.push('/dashboard');

        /*
        fetch('http://chat-server-challange.herokuapp.com/contacts', {
          credentials: 'include', 
        })
        .then(response => response.json())
        .then(data => {
          console.log(data) 
        })
        .catch(error => console.error(error))
      */

      })
      .catch(error => console.error(error));

  }

  handleUserChange(evt) {
    this.setState({
      username: evt.target.value,
    });
  };

  handlePassChange(evt) {
    this.setState({
      password: evt.target.value,
    });
  }

  render() {


    return (
      /*
      <div className="wrapper">
        <div className="container">
        */
<div className="outer-container">
   <div className="inner-container">
     <div className="centered-content">     
          <form onSubmit={this.handleSubmit}>
            {
              this.state.error &&
              <h3 data-test="error" onClick={this.dismissError}>
                <button onClick={this.dismissError}>✖</button>
                {this.state.error}
              </h3>
            }
            <label for="username">Username</label>
            <input type="text" className="input" name="username" placeholder="Enter Username" value={this.state.username} onChange={this.handleUserChange} required />

            <label for="pass" >Password</label>
            <input type="password" className="input" name="pass" placeholder="Enter Password" value={this.state.password} onChange={this.handlePassChange} required />

            <input type="submit" value="Submit" className="button" />
          </form>
        </div>
      </div>

      </div>  
    );
  }
}

export default Login;
