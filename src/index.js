import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import Dashboard from "./components/Dashboard";
import Login from "./components/Login";
import ContactDetail from "./components/ContactDetail";
import { Switch, Route, BrowserRouter } from "react-router-dom";


const routes = (

  <BrowserRouter>

    <Switch>

      <Route exact path="/" component={Login} />

      <Route path="/dashboard" component={Dashboard} />

      <Route path="/detail" component={ContactDetail} />

    </Switch>

  </BrowserRouter>

);

//ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(routes, document.getElementById("root"));




// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
